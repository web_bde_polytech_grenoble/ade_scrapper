import base64
import json
from google.cloud.firestore_v1.document import DocumentReference
from google.cloud.firestore_v1 import CollectionReference
from google.cloud import firestore
import firebase_admin as admin
import firebase_admin.firestore
from typing import Dict, List, Tuple, Union
import os
import requests
from queue import Queue
import logging
import ics

cred = json.loads(base64.b64decode(os.environ['FIREBASE_SERVICE_ACCOUNT']))
admin.initialize_app(credential=admin.credentials.Certificate(cred))
client: firestore.Client = firebase_admin.firestore.client()


def getCollection(name: str) -> CollectionReference:
    return client.collection(name)


def getDocumentData(col_name: str, doc_name: str) -> Union[Dict[str, any], None]:
    doc: DocumentReference = getCollection(col_name).document(doc_name)
    return doc.get().to_dict()


def setDocument(col_name: str, doc_name: str, data: Dict[str, any], with_timestamp=True):
    doc: DocumentReference = getCollection(
        col_name).document(doc_name)
    if with_timestamp:
        data["updated"] = firestore.SERVER_TIMESTAMP
    return doc.set(data)


def parseCal(data: str) -> List[Dict[str, str]]:
    return {
        e.uid: {
            "description": e.description.split("(Exp")[0],
            "location": e.location,
            "summary": e.name,
            "start": e.begin.datetime,
            "end": e.end.datetime,
        } for e in list(ics.Calendar(data).events)
    }


def get_cal(cal_id: str) -> str:
    url = os.environ["ADE_URL"]+cal_id+os.environ["ADE_TIME"]
    response = requests.get(url)
    if response.headers['content-type'] != "text/calendar;charset=UTF-8":
        raise TypeError("HTTP Response not a Calendar : " +
                        response.headers["content-type"])
    return response.text


def flatten(dictionary: Dict[str, any]) -> List[Tuple[str, str]]:
    items = []
    for a in dictionary.items():
        items.extend([a] if isinstance(a[1], str) else flatten(a[1]))
    return items


if __name__ == "__main__":

    logging.basicConfig(
        format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)
    resources = getDocumentData(os.environ["RES_COLLECTION"], "resources")
    queue = Queue()
    for r in flatten(resources):
        queue.put(r)

    counter = 0
    maxcount = len(resources)
    while not queue.empty():
        name, cal_id = queue.get()
        logging.info("Updating {}...".format(name))
        try:
            setDocument(os.environ["CAL_COLLECTION"], cal_id, {
                "events": parseCal(get_cal(cal_id)),
                "name": name
            })
            counter += 1
            logging.info(str(counter)+"/"+str(maxcount))
        except Exception as e:
            logging.warning("Error, back to queue, "+str(e))
            queue.put(name, cal_id)
